<?php session_start();
?>

<aside class="admin-sidebar">
<div class="admin-sidebar-brand">
<!-- begin sidebar branding-->
<!--<img class="admin-brand-logo" src="assets/img/Loyaltician Logo.jpg" width="40" alt="atmos Logo">-->
<span class="admin-brand-content font-secondary"><a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/dashboard_admin.php">Customer Relationship Management</a></span>
<!-- end sidebar branding-->
<div class="ml-auto">
<!-- sidebar pin-->
<a href="#" class="admin-pin-sidebar btn-ghost btn btn-rounded-circle"></a>
<!-- sidebar close for mobile device-->
<a href="#" class="admin-close-sidebar"></a>
</div>
</div>

<div class="admin-sidebar-wrapper js-scrollbar">
<ul class="menu">
<li class="menu-item ">
<a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/dashboard_admin.php" class=" menu-link" id="hdm1">
<span class="menu-label">
<span class="menu-name"  >Dashboard
<span class="menu-arrow"></span>
</span>
</span>
<span class="menu-icon">
<span class="icon-badge badge-success badge badge-pill">1</span>
<i class="icon-placeholder mdi mdi-shape-outline "></i>
</span>
</a>
</li>





<li class="menu-item" >
<a href="#" class="open-dropdown menu-link"  style="">
<span class="menu-label">
<span class="menu-name">Master
<span class="menu-arrow"></span>
</span>

</span>
<span class="menu-icon">
<i class="icon-placeholder mdi mdi-lead-pencil "></i>
</span>
</a>
<!--submenu-->
<ul class="sub-menu" style="display: none;">


<li class="menu-item" id="45" style="">
        <a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/master/new_role.php" class=" menu-link">
        <span class="menu-label">
        <span class="menu-name">Add New Role
        </span>
        </span>
        <span class="menu-icon">

        <i class="icon-placeholder mdi mdi-checkbook "></i>
        </span>
        </a>
        </li>


<!--         <li class="menu-item" id="45" style="">
        <a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/master/new_master.php" class=" menu-link">
        <span class="menu-label">
        <span class="menu-name">Add New master
        </span>
        </span>
        <span class="menu-icon">

        <i class="icon-placeholder mdi mdi-checkbook "></i>
        </span>
        </a>
        </li>


        <li class="menu-item" id="49" style="">
        <a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/master/view_master.php" class=" menu-link">
        <span class="menu-label">
        <span class="menu-name">View User 
        </span>
        </span>
        <span class="menu-icon">

        <i class="icon-placeholder mdi mdi-checkbook "></i>
        </span>
        </a>
        </li> -->

</ul>
</li>



<li class="menu-item">
<a href="#" class="open-dropdown menu-link"  style="">
<span class="menu-label">
<span class="menu-name">New Customer Lead
<span class="menu-arrow"></span>
</span>

</span>
<span class="menu-icon">
<i class="icon-placeholder mdi mdi-lead-pencil "></i>
</span>
</a>
<!--submenu-->
<ul class="sub-menu" style="display: none;">

        <li class="menu-item" id="45" style="">
        <a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/customer/new_customer.php" class=" menu-link">
        <span class="menu-label">
        <span class="menu-name">Add New Customer Lead
        </span>
        </span>
        <span class="menu-icon">

        <i class="icon-placeholder mdi mdi-checkbook "></i>
        </span>
        </a>
        </li>


        <li class="menu-item" id="49" style="">
        <a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/customer/view_customer.php" class=" menu-link">
        <span class="menu-label">
        <span class="menu-name">View Customer 
        </span>
        </span>
        <span class="menu-icon">

        <i class="icon-placeholder mdi mdi-checkbook "></i>
        </span>
        </a>
        </li>

</ul>
</li>
















<li class="menu-item">
<a href="#" class="open-dropdown menu-link"  style="">
<span class="menu-label">
<span class="menu-name">Sales Team
<span class="menu-arrow"></span>
</span>

</span>
<span class="menu-icon">
<i class="icon-placeholder mdi mdi-lead-pencil "></i>
</span>
</a>
<!--submenu-->
<ul class="sub-menu" style="display: none;">

        <li class="menu-item" id="45" style="">
        <a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/sales/add_sales_team.php" class=" menu-link">
        <span class="menu-label">
        <span class="menu-name">Add New Member
        </span>
        </span>
        <span class="menu-icon">

        <i class="icon-placeholder mdi mdi-checkbook "></i>
        </span>
        </a>
        </li>


        <li class="menu-item" id="49" style="">
        <a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/sales/view_sales_team.php" class=" menu-link">
        <span class="menu-label">
        <span class="menu-name">View Member 
        </span>
        </span>
        <span class="menu-icon">

        <i class="icon-placeholder mdi mdi-checkbook "></i>
        </span>
        </a>
        </li>

</ul>
</li>




<li class="menu-item">
<a href="#" class="open-dropdown menu-link"  style="">
<span class="menu-label">
<span class="menu-name">Activity
<span class="menu-arrow"></span>
</span>

</span>
<span class="menu-icon">
<i class="icon-placeholder mdi mdi-lead-pencil "></i>
</span>
</a>
<!--submenu-->
<ul class="sub-menu" style="display: none;">

        <li class="menu-item" id="45" style="">
        <a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/activity/new_activity.php" class=" menu-link">
        <span class="menu-label">
        <span class="menu-name">Add New Activity
        </span>
        </span>
        <span class="menu-icon">

        <i class="icon-placeholder mdi mdi-checkbook "></i>
        </span>
        </a>
        </li>


        <li class="menu-item" id="49" style="">
        <a href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/activity/view_activity.php" class=" menu-link">
        <span class="menu-label">
        <span class="menu-name">View Customer 
        </span>
        </span>
        <span class="menu-icon">

        <i class="icon-placeholder mdi mdi-checkbook "></i>
        </span>
        </a>
        </li>

</ul>
</li>




</ul>

</div>

</aside>