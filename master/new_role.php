<?php include($_SERVER["DOCUMENT_ROOT"]."/CRM/side-top.php");?>


<div class="container">
	


					<div class="card m-b-30">
                        <div class="card-header">
                            <h5 class="m-b-0">
                                 Add New Roles
                            </h5>

                        </div>


                        <form action="<?php $_SERVER["DOCUMENT_ROOT"]?>/CRM/master/new_role_process.php" method="POST">
                        	


                        <div class="card-body ">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="Role">Role Name</label>
                                    <input type="text" class="form-control" id="role_name" name="role_name" placeholder="Role Name">
                                </div>
                            </div>

                      <div class="col-12 form-group">
                           <label for="validationServerUsername">Permission</label>
							<div>
								<label class="cstm-switch">
                                    <input type="checkbox" checked="" name="user_perm_add" value="1" class="cstm-switch-input">
                                    <span class="cstm-switch-indicator "></span>
                                    <span class="cstm-switch-description">Add</span>
                                </label>

                                <label class="cstm-switch">
                                    <input type="checkbox" checked="" name="user_perm_update" value="1" class="cstm-switch-input">
                                    <span class="cstm-switch-indicator bg-success "></span>
                                    <span class="cstm-switch-description">Update </span>
                                </label>

                                <label class="cstm-switch">
                                    <input type="checkbox" checked="" name="user_perm_delete" value="1" class="cstm-switch-input">
                                    <span class="cstm-switch-indicator bg-danger "></span>
                                    <span class="cstm-switch-description">Delete </span>
                                </label>

							</div>
                         </div>



                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>


                        </form>


                    </div>










</div>
</main>

</body>
</html>
