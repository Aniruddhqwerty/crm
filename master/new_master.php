<?php include($_SERVER["DOCUMENT_ROOT"]."/CRM/side-top.php");?>
<div class="container">
<div class="card m-b-30">
                        <div class="card-header">
                            <h5 class="m-b-0">
                                 Add New User
                            </h5>

                        </div>
                        <div class="card-body ">
                            

                            <form method="POST" action="<?php $_SERVER["DOCUMENT_ROOT"]?>/CRM/user/new_user_process.php">
                                <div class="form-row">
                                    <div class="col-md-4 mb-3">
                                        <label for="validationServer01">First name</label>
                                        <input type="text" class="form-control" id="user_fname" placeholder="First name" name="user_fname" >
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="validationServer02">Last name</label>
                                        <input type="text" class="form-control" id="user_lname" placeholder="Last name" name="user_lname" >
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="validationServerUsername">Email</label>
                                        <input type="text" class="form-control" id="user_email" placeholder="Email" name="user_email" >
                                        
                                    </div>
                                </div>

                                <div class="form-row">
	                              
	                              <div class="col-6">
	                              	<label for="validationServer02">Mobile</label>
                                        <input type="number" class="form-control" id="validationServer02" placeholder="Mobile">
	                              </div>

	                                <div class="col-6 form-group">
	                                    
	                                    <label for="validationServerUsername">Roles</label>

	                                    <select class="form-control" name="user_role">
	                                        <option>Super Admin</option>
	                                        <option>Admin</option>
	                                        <option>Manager</option>
	                                        <option>Account</option>
	                                    </select>
	                                </div>

                                	
                                </div>
                                

                      <div class="col-12 form-group">
                                        <label for="validationServerUsername">Permission</label>
							<div>
								<label class="cstm-switch">
                                    <input type="checkbox" checked="" name="user_perm_add" value="1" class="cstm-switch-input">
                                    <span class="cstm-switch-indicator "></span>
                                    <span class="cstm-switch-description">Add</span>
                                </label>

                                <label class="cstm-switch">
                                    <input type="checkbox" checked="" name="user_perm_update" value="1" class="cstm-switch-input">
                                    <span class="cstm-switch-indicator bg-success "></span>
                                    <span class="cstm-switch-description">Update </span>
                                </label>

                                <label class="cstm-switch">
                                    <input type="checkbox" checked="" name="user_perm_delete" value="1" class="cstm-switch-input">
                                    <span class="cstm-switch-indicator bg-danger "></span>
                                    <span class="cstm-switch-description">Delete </span>
                                </label>

							</div>
                         </div>




                                <button class="btn btn-primary" type="submit">Create User</button>
                            </form>
                        </div>
                    </div>




</main>

</body>
</html>
