 <meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<title>CRM</title>
<link rel="icon" type="image/x-icon" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/img/logo.png"/>
<link rel="icon" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/img/logo.png" type="image/png" sizes="16x16">
<link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/pace/pace.css">
<script src="assets/vendor/pace/pace.min.js"></script>
<!--vendors-->
<link rel="stylesheet" type="text/css" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/jquery-scrollbar/jquery.scrollbar.css">
<link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/select2/css/select2.min.css">
<link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/timepicker/bootstrap-timepicker.min.css">
<link href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/https://fonts.googleapis.com/css?family=Hind+Vadodara:400,500,600" rel="stylesheet">
<link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/fonts/jost/jost.css">
<!--Material Icons-->
<link rel="stylesheet" type="text/css" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/fonts/materialdesignicons/materialdesignicons.min.css">
<!--Bootstrap + atmos Admin CSS-->
<link rel="stylesheet" type="text/css" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/css/atmos.min.css">
<!-- Additional library for page -->
    <link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/datedropper/datedropper.min.css">
    <link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/dropzone/dropzone.css">


<link rel="stylesheet" type="text/css" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">







<!--Material Icons-->
<link rel="stylesheet" type="text/css" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/fonts/materialdesignicons/materialdesignicons.min.css">
<!--Bootstrap + atmos Admin CSS-->
<link rel="stylesheet" type="text/css" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/css/atmos.min.css">
<!-- Additional library for page -->
 <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  
      <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.2/sweetalert2.all.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
 <link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<link rel="stylesheet" href="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/aniruddh/style.css">

<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/popper/popper.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/select2/js/select2.full.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/listjs/listjs.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/moment/moment.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/daterangepicker/daterangepicker.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/js/atmos.min.js"></script>
<!--page specific scripts for demo-->
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/vendor/DataTables/datatables.min.js"></script>
<script src="<?php include($_SERVER["DOCUMENT_ROOT"]);?>/CRM/assets/js/datatable-data.js"></script>








<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
} 

</script>
