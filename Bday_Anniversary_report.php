<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
 <?php include("header.php")?>
<!-- Additional library for page -->
    <link rel="stylesheet" href="assets/vendor/DataTables/datatables.min.css">
    <link rel="stylesheet" href="assets/vendor/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
<script>
    
function searchfiltter(){
    
    var Ab_Filtter=document.getElementById('Ab_Filtter').value;
    var FromDt=document.getElementById('FromDt').value;
    var Todt=document.getElementById('Todt').value;
   
      if(Ab_Filtter==""){
          swal("Please Select Dropdown");
      }else if(FromDt==""){
          swal("Please Select From Date");
      }else if(Todt==""){
          swal("Please Select To Date");
      }else{
     
             $.ajax({
          
                    type:'POST',
                    url:'Bday_Anniversary_filtter.php',
                     data:'FromDt='+FromDt+'&Todt='+Todt+'&Ab_Filtter='+Ab_Filtter,
                    
                    success:function(msg){
                     // alert(msg);
                        $('#setTable').empty();
                             var json=$.parseJSON(msg);
                             for(var i=0;i<json.length;++i){
                          //  alert(json[i].FirstName)
                            
                           
                            var srno=i+1;
                            var Birth_Anni="";
                            if(Ab_Filtter=="Birthday"){ if(json[i].Primary_DateOfBirth=='01-01-1970'){ Birth_Anni="00-00-0000"; }else{Birth_Anni=json[i].Primary_DateOfBirth;}     }else if(Ab_Filtter=="Anniversary"){  if(json[i].Primary_Anniversary=='01-01-1970'){ Birth_Anni="00-00-0000"; }else{   Birth_Anni=json[i].Primary_Anniversary;} }
                         $('#BdayAnni').text(Ab_Filtter);
                          $('#BdayAnni2').text(Ab_Filtter);
                    //$('#setTable').append('<tr role="row" class="odd" ><td class="sorting_1">'+srno+'</td><td>'+json[i].Primary_nameOnTheCard+'</td><td>'+json[i].Type+'</td><td>'+json[i].level_name+'</td><td>'+json[i].GenerateMember_Id+'</td><td>'+json[i].entryDate+'</td><td>'+json[i].R+'</td><td>'+json[i].Primary_Anniversary+'</td><td>'+json[i].Primary_DateOfBirth+'</td><td>'+json[i].booklet_Series+'</td><td>'+json[i].TypeNR+'</td><td>'+json[i].MembershipDts_PaymentMode+'</td><td>'+json[i].MembershipDts_InstrumentNumber+'</td><td>'+json[i].Member_bankName+'</td><td>'+json[i].Recipt+'</td> <td>'+json[i].MembershipDts_NetPayment +'</td>  <td>'+json[i].MembershipDts_GST +'</td> <td>'+json[i].MembershipDts_GrossTotal +'</td><td>'+json[i].MemshipDts_Remarks +'</td> </tr>');
                               $('#setTable').append('<tr role="row" class="odd" ><td class="sorting_1">'+srno+'</td><td>'+json[i].Primary_nameOnTheCard+'</td><td>'+json[i].Type+'</td><td>'+json[i].level_name+'</td><td>'+json[i].GenerateMember_Id+'</td><td>'+Birth_Anni+'</td><td>'+json[i].booklet_Series+'</td><td>'+json[i].EmailId +'</td><td>'+json[i].MobileNumber +'</td><td>'+json[i].MemshipDts_Remarks +'</td> </tr>');
                      
                          }
                     
                      document.getElementById('qr').value="";
                      document.getElementById('qr').value=json['0'].Qry;
                      
                      document.getElementById('qr1').value="";
                      document.getElementById('qr1').value=json['0'].Qry;
                       document.getElementById('From1').value="";
                      document.getElementById('From1').value=json['0'].FromDat;
                       document.getElementById('To1').value="";
                      document.getElementById('To1').value=json['0'].Todt;
               
                   
                    }
                })
    
     
      }
}


 $( function() {
    $( "#FromDt" ).datepicker();
        $( "#FromDt" ).datepicker( "option", "dateFormat", "dd-mm" );
        $( "#FromDt" ).datepicker( "option", "showAnim", "fold" ); 
        $( "#FromDt" ).datepicker( "option", "changeMonth", "true" );
        
  } );
  
   $( function() {
    $( "#Todt" ).datepicker();
        $( "#Todt" ).datepicker( "option", "dateFormat", "dd-mm" );
        $( "#Todt" ).datepicker( "option", "showAnim", "fold" ); 
        $( "#Todt" ).datepicker( "option", "changeMonth", "true" );
        
  } );
</script>
<!--========= hide year in datepicker==================-->
<style>
    .ui-datepicker-year
{
 display:none;   
    /* clip: rect(0px, 243px, 219px, 0px);*/
}.ui-datepicker{clip: rect(0px, 243px, 235px, 0px) !important};
</style>
<!--==================================================-->
</head>
<body class="sidebar-pinned">


<?php include("vertical_menu.php")?>
<main class="admin-main">
  <?php include('navbar.php');?>
  
    <section class="admin-content">
        <div class="bg-dark">
            <div class="container  m-b-30">
                <div class="row">
                    <div class="col-12 text-white p-t-40 p-b-90">

                        <h4 class=""> <span class="btn btn-white-translucent">
                                <i class="mdi mdi-table "></i></span> view Birthday/Anniversary 
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="container  pull-up">
            <div class="row">
                <div class="col-12">
                    <div class="card">
<?php include("config.php");
  //  $View="select * from Leads_table where leadEntryef='".$_SESSION['id']."'";
 	  $View="select * from Members where DATE(entryDate)='".date("Y-m-d")."'";
 	
      $qrys=mysqli_query($conn,$View);

?>
                        <div class="card-body">
                            
                                <div class="form-row">
                               <div class="form-group col-md-3">
                                  
                                  <select class="form-control" name="Ab_Filtter" id="Ab_Filtter" >
                                  <option value="">Select</option>
                                  <option value="Anniversary">Anniversary</option>
                                  <option value="Birthday">Birthday</option>
                                  </select>
                                  
                                  </div>
                                  
                                   <div class="form-group col-md-3">
                                      <!-- <select class="form-control" name="FromDt" id="FromDt" >
                                  <option value="">Select From Date</option>
                                  <option value="1">January</option>
                                  <option value="2">February</option>
                                   <option value="3">March</option>
                                  <option value="4">April</option>
                                   <option value="5">May</option>
                                  <option value="6">June</option>
                                   <option value="7">July</option>
                                  <option value="8">August</option>
                                   <option value="9">September</option>
                                   <option value="10">October</option>
                                  <option value="11">November</option>
                                   <option value="12">December</option>
                                  </select>-->
                                       
                                 <!-- <input type="text" class="js-datepicker form-control" id="FromDt" name="FromDt" placeholder="From Date">-->
                                 <input type="text" class="form-control" id="FromDt" name="FromDt" autocomplete="off" placeholder="From Date">
                                  </div><div class="form-group col-md-3">
                                   
                                  <!-- <select class="form-control" name="Todt" id="Todt" >
                                  <option value="">Select To date</option>
                                  <option value="1">January</option>
                                  <option value="2">February</option>
                                   <option value="3">March</option>
                                  <option value="4">April</option>
                                   <option value="5">May</option>
                                  <option value="6">June</option>
                                   <option value="7">July</option>
                                  <option value="8">August</option>
                                   <option value="9">September</option>
                                   <option value="10">October</option>
                                  <option value="11">November</option>
                                   <option value="12">December</option>
                                  </select>-->
                                   
                                <!-- <input type="text" class="js-datepicker form-control" id="Todt" name="Todt" placeholder="To Date">-->
                                 <input type="text" class="form-control" id="Todt" autocomplete="off" name="Todt" placeholder="To Date">
                                  </div><div class="form-group col-md-3">
                                   <input type="button" class="btn btn-primary" onclick="searchfiltter()" value="Search">
                               </div>
                             
                              </div>
                            
                            
                            
                            <div class="table-responsive p-t-10">
                                <table id="example" class="table" style="width:100%">
                                    <thead>
                                    <tr>
                                     <th>srno</th>  
                                     <th> Name on the Card</th> 
                                     <th> Type</th> 
                                     <th> Level</th>
                                     <th> Membership No.</th>
                                     <!--<th> DSR_Date</th>
                                     <th> Expiry Date</th>-->
                                     <th id="BdayAnni"> Anniversary Date</th>
                                    <!-- <th> DOB_Date</th>-->
                                     <th> Booklet Number</th>
                                  <!--    <th> Type(N/R)</th>
                                     <th> Payment Mode</th>
                                     <th> Instrument No.</th>
                                     <th> Authorisation</th>
                                      <th> Receipt No.</th>
                                      <th> Amount</th>
                                      <th> GST</th>
                                      <th> Total Amount</th>-->
                                      <th>Email</th>
                                      <th>Mobile</th>
                                      <th> Remarks</th>

                                     
                                         
                                          
                                    </tr>
                                    </thead>
                                    <tbody id="setTable">
                                        
                                               	<?php 
	/*	$srn=1;
			while($_row=mysqli_fetch_array($qrys))
			{
	$sql2="select * from Leads_table where Lead_id='".$_row['Static_LeadID']."' ";

	$runsql2=mysqli_query($conn,$sql2);
	$sql2fetch=mysqli_fetch_array($runsql2);
	
	
	$sql3="SELECT * FROM `Level` where Leval_id='".$_row['MembershipDetails_Level']."' ";
	$runsql3=mysqli_query($conn,$sql3);
	$sql3fetch=mysqli_fetch_array($runsql3);
	
	$sql4="SELECT Expiry_month FROM `validity` where Leval_id='".$_row['MembershipDetails_Level']."' ";
	$runsql4=mysqli_query($conn,$sql4);
	$sql4fetch=mysqli_fetch_array($runsql4);
	
	
	$sql5="SELECT state FROM `state` where state_id='".$sql2fetch['State']."' ";
	$runsql5=mysqli_query($conn,$sql5);
	$sql5fetch=mysqli_fetch_array($runsql5);
	
	
	$dd=date('Y-m-d', strtotime($_row['entryDate']));

	 $d = strtotime("+".$sql4fetch['Expiry_month']." months",strtotime($dd));
      $R=  date("d-m-Y",$d);
*/
  ?>
                             <tr>
                                 
                               
                                 
                                    <td><?php echo $srn;?></td>
                                 	<td><?php echo $_row['Primary_nameOnTheCard']; ?></td>
                                 	<td><?php echo ''; ?></td>
                                    <td><?php echo $sql3fetch['level_name']; ?></td>
                                    <td><?php echo $_row['GenerateMember_Id']; ?></td>
                                   <!-- <td><?php echo $dd; ?></td>
                                   	<td><?php echo $R; ?></td>-->
                                   	<td><?php echo $_row['Primary_Anniversary']; ?></td>
                                   	<!--<td><?php echo $_row['Primary_DateOfBirth']; ?></td>-->
                                   	
                                   	
                                   	<td><?php echo $_row['booklet_Series']; ?></td>
                                  <!-- 	<td><?php echo 'New'; ?></td>
                                    <td><?php echo $_row['MembershipDts_PaymentMode'] ; ?></td>
                                    <td><?php echo $_row['MembershipDts_InstrumentNumber'] ; ?></td>
                                    <td><?php echo $_row['Member_bankName'];?></td>
                                    <td><?php echo '';?></td>
                                    <td><?php echo $_row['MembershipDts_NetPayment']; ?></td>
                                     <td><?php echo $_row['MembershipDts_GST']; ?></td>
                                    <td><?php echo $_row['MembershipDts_GrossTotal']; ?></td>-->
                                    <td><?php echo $sql2fetch['EmailId']; ?></td>
                                    <td><?php echo $sql2fetch['MobileNumber']; ?></td>
                                      <td><?php echo $_row['MemshipDts_Remarks']; ?></td>
   
   
   
   <!-- 
	<td><?php echo $_row['GenerateMember_Id']; ?></td>
	<td><?php echo $_row['Primary_Title']; ?></td>
	<td><?php echo $sql2fetch['FirstName']; ?></td>
	<td><?php echo $sql2fetch['LastName']; ?></td>
	<td><?php echo $_row['Primary_nameOnTheCard']; ?></td>

	<td><?php echo $_row['Spouse_FirstName']; ?></td>
	<td><?php echo $sql2fetch['MobileNumber']; ?></td>
	<td><?php echo $sql3fetch['level_name']; ?></td>
	<td><?php echo $_row['entryDate']; ?></td>
	<td><?php echo $sql4fetch['Expiry_month']." ".Month;?></td>

	<td><?php echo $_row['Primary_mob1']; ?></td>
	<td><?php echo $_row['Primary_Contact1']; ?></td>
	<td><?php echo $_row['Primary_Contact2']; ?></td>
	<td><?php echo $_row['Primary_Contact3']; ?></td>
	<td><?php echo $_row['Primary_Email_ID2']; ?></td>

	<td><?php echo $_row['Spouse_GmailMArrid1']; ?></td>
	<td><?php echo $sql2fetch['Company']; ?></td>
	<td><?php echo $sql2fetch['Designation']; ?></td>
	<td><?php echo $_row['Primary_AddressType1']; ?></td>
	<td><?php echo $_row['Primary_BuldNo_addrss'].$_row['Primary_Area_addrss'].$_row['Primary_Landmark_addrss']; ?></td>
	<td><?php echo $sql2fetch['City']; ?></td>
	<td><?php echo $sql5fetch['state']; ?></td>
	<td><?php echo $sql2fetch['Country']; ?></td>
	<td><?php echo $sql2fetch['PinCode']; ?></td>

	<td><?php echo $_row['Primary_DateOfBirth']; ?></td>
	<td><?php echo $_row['Primary_MaritalStatus']; ?></td>
   <td></td>	-->

	



	

				</tr>
			
			<?php 
			
			   $srn++;
		//	}			
			?>
	
                                
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                       <th>srno</th>  
                                     <th> Name on the Card</th> 
                                     <th> Type</th> 
                                     <th> Level</th>
                                     <th> Membership No.</th>
                                     <!--<th> DSR Date</th>
                                     <th> Expiry Date</th>-->
                                     <th id="BdayAnni2"> Anniversary Date</th>
                                   <!--  <th> DOB Date</th>-->
                                     <th> Booklet Number</th>
                                    <!-- <th> Type(N/R)</th>
                                     <th> Payment Mode</th>
                                     <th> Instrument No.</th>
                                     <th> Authorisation</th>
                                      <th> Receipt No.</th>
                                      <th> Amount</th>
                                      <th> GST</th>
                                      <th> Total Amount</th>-->
                                      <th>Email</th>
                                      <th>Mobile</th>
                                      <th> Remarks</th>
                                     
                                         </tr>
                                    </tfoot>
                                </table>
                            </div>
              <!--           <div class="row">   
            <div class="cols-md-8">
        <form name="frm" method="post" action="exportDSR.php" target="_new">
<input type="hidden" name="qr" id="qr" value="<?php echo $View; ?>" readonly>
<input type="submit" name="cmdsub" value="Export" class="btn btn-primary"> <span>(From here you can Export MAX 860 Record at one Time.)</span>
</form>
</div>&nbsp;&nbsp;
 <div class="cols-md-4">
<form name="frm" method="post" action="Leadpdf/DSRreportPDF.php" target="_new">
<input type="hidden" name="qr1" id="qr1" value="<?php echo $View; ?>" readonly>
<input type="hidden" name="From1" id="From1"  readonly>
<input type="hidden" name="To1" id="To1"  readonly>
<input type="submit" name="cmdsub" value="Generate PDF" class="btn btn-primary">
</form>
</div></div>-->
        
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
         
        
         
        
    </section>

</main>
<?php include('belowScript.php');?><script src="assets/vendor/DataTables/datatables.min.js"></script>
<!--<script src="assets/js/datatable-data.js"></script>-->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</body>
</html>