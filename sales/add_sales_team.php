<?php include($_SERVER["DOCUMENT_ROOT"]."/CRM/side-top.php");?>



<div class="container">
	
<div class="card m-b-30">
                        <div class="card-header">
                            <h5 class="m-b-0">
                                 New Sales Team
                            </h5>

                        </div>

                        <form action="sales_team_process.php" method="POST">
                        	
                        <div class="card-body ">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name">Sales Person Name</label>
                                    <input type="text" class="form-control" name="exe_name" id="exe_name" placeholder="Sales Person Name" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="level">Sales Person Level</label>
									<select class="form-control" name="exe_title" id="exe_title" >

<?php $sql = "SELECT * FROM roles";
        if ($result = $conn -> query($sql)) {
            while ($row = $result -> fetch_assoc()) { ?>    
                <option><?php echo ucwords($row['role']); ?></option>
  <?php }
  $result -> free_result();
} ?>



									</select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
								
                                <label for="designation">Designation</label>
                                <input type="text" class="form-control" name="exe_desgn" id="exe_desgn" placeholder="designation">
                            </div>

                                <div class="form-group col-md-6">
                            
                                <label for="inputAddress2">Employee ID</label>
                                <input type="text" class="form-control" id="exe_empid" placeholder="employeeID" name="exe_empid">
                            </div>
</div>                            

                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="location">Location</label>
                                    <input type="text" class="form-control" id="exe_loc" name="exe_loc">


                                </div>

                                <div class="form-group col-md-6">
                                    <label for="mobile">Mobile</label>
                                    <input type="number" class="form-control" id="exe_contact" name="exe_contact">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="exe_email" name="exe_email">


                                </div>

                                <div class="form-group col-md-3">
                                    <label for="team_head">Team Head</label>
                                    <input type="text" class="form-control" id="sales_head" name="sales_head">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="reports_to">Reports To</label>
                                    <input type="text" class="form-control" id="sales_rep" name="sales_rep">
                                </div>
                            </div>



                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        </form>


                    </div>


</div>


</main>


exe_title
<script>

$(window).ready(function() {

    $('form').submit(function() {
        if ($('#exe_desgn').val() == '' || $('#exe_name').val() == '' || $('#sales_head').val() == '' || $('#sales_rep').val() == '' || $('#exe_title').val() == '' || $('#exe_loc').val() == '') {
            
            if ($('#exe_name').val() == '') {
                $('#exe_name').addClass('is-invalid');
            }

             if ($('#exe_title').val() == '') {
                $('#exe_title').addClass('is-invalid');
            }

                        if ($('#exe_desgn').val() == '') {
                $('#exe_desgn').addClass('is-invalid');
            }

                        if ($('#exe_loc').val() == '') {
                $('#exe_loc').addClass('is-invalid');
            }


             if ($('#sales_head').val() == '') {
                $('#sales_head').addClass('is-invalid');
            }

             if ($('#sales_rep').val() == '') {
                $('#sales_rep').addClass('is-invalid');
            }
            swal('please enter valid Input');

            return false;
        }

        return true;
    });
});


$(window).ready(function() {
setTimeout(function() { 

    $('form').submit(function() {
        if ($('#exe_contact').val().length != 10 ) {

            if ($('#exe_contact').val() == '') {
                $('#exe_contact').addClass('is-invalid');
            }

            swal('please enter valid number');

return false;
        }
        return true;
    });
}, 2000);
    
});


</script>
</body>
</html>
