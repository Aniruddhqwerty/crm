<?php include($_SERVER["DOCUMENT_ROOT"]."/CRM/side-top.php");?>



<div class="container">
	
<div class="card m-b-30">
                        <div class="card-header">
                            <h5 class="m-b-0">
                                 Edit Sales Team Member
                            </h5>

                        </div>


<?php 
 $id=$_GET['id'];

$sql = "SELECT * FROM salesteam WHERE exe_id=$id";

if ($result = $conn -> query($sql)) {
  while ($row = $result -> fetch_assoc()) {



?>    






                        <form action="<?php $_SERVER["DOCUMENT_ROOT"]?>/CRM/sales/update_sales_team.php?id=<?php echo $id; ?>" method="POST">
                        	
                        <div class="card-body ">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name">Sales Person Name</label>
                                    <input type="text" class="form-control" name="exe_name" id="exe_name" value="<?php echo $row['exe_name']; ?>" placeholder="Sales Person Name" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="level">Sales Person Level</label>
                                    <?php $row['exe_title']; ?>
                                    <select class="form-control" name="exe_title" id="exe_title" required>
										<option>Sales Exec</option>
										<option>Team Head</option>
										<option>Regional Head</option>
										<option>Head</option>
									</select>
<script>
$("select option").each(function(){
  if ($(this).text() == "<?php echo $row['exe_title']?>")
    $(this).attr("selected","selected");
});
</script>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
								
                                <label for="designation">Designation</label>
                                <input type="text" class="form-control" name="exe_desgn" id="exe_desgn" value="<?php echo $row['exe_desgn']; ?>" placeholder="designation">
                            </div>

                                <div class="form-group col-md-6">
                            
                                <label for="inputAddress2">Employee ID</label>
                                <input type="text" class="form-control" id="exe_empid" placeholder="employeeID" name="exe_empid" value="<?php echo $row['exe_empid']; ?>">
                            </div>
</div>                            

                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="location">Location</label>
                                    <input type="text" class="form-control" id="exe_loc" name="exe_loc" value="<?php echo $row['exe_loc']; ?>">


                                </div>

                                <div class="form-group col-md-6">
                                    <label for="mobile">Mobile</label>
                                    <input type="number" class="form-control" id="exe_contact" name="exe_contact" value="<?php echo $row['exe_contact']; ?>">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="exe_email" name="exe_email" value="<?php echo $row['exe_email']; ?>">


                                </div>

                                <div class="form-group col-md-3">
                                    <label for="team_head">Team Head</label>
                                    <input type="text" class="form-control" id="sales_head" value="<?php echo $row['sales_head']; ?>" name="sales_head">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="reports_to">Reports To</label>
                                    <input type="text" class="form-control" id="sales_rep" value="<?php echo $row['sales_rep']; ?>" name="sales_rep">
                                </div>
                            </div>



                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        </form>



  <?php }  $result -> free_result();
} ?>



                    </div>


</div>


</main>
</body>
</html>
