<?php include($_SERVER["DOCUMENT_ROOT"]."/CRM/side-top.php");?>



<div class="container">
	
<div class="card m-b-30">
                        <div class="card-header">
                            <h5 class="m-b-0">
                                 Edit Activity
                            </h5>

                        </div>

                        <?php 
 $id=$_GET['id'];

$sql = "SELECT * FROM activity WHERE id=$id";

if ($result = $conn -> query($sql)) {
  while ($row = $result -> fetch_assoc()) {



?>    




                        <form action="<?php $_SERVER["DOCUMENT_ROOT"]?>/CRM/activity/update_activity.php?id=<?php echo $id; ?>" method="POST">
                        	
                        <div class="card-body ">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name">Customer Name</label>
                                    <input type="text" class="form-control" name="act_name" id="act_name" placeholder="Customer Name" value="<?php echo $row['act_name']; ?>" disabled>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="level">Branch / Office Name</label>
									<select class="form-control" name="act_branch_name" id="act_branch_name" >

										<option>Sales Exec</option>
										<option>Team Head</option>
										<option>Regional Head</option>
										<option>Head</option>
									</select>


                                    <script>
                  

                                    </script>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
								
                                <label for="designation">Customer Type</label>
                                <input type="text" class="form-control" name="act_cust_type"  value="<?php echo $row['act_cust_type']; ?>" id="act_cust_type" placeholder="Customer Type">
                            </div>

                                <div class="form-group col-md-6">
                            
                                <label for="inputAddress2">City</label>
                                <input type="text" class="form-control" id="act_city" placeholder="City" name="act_city" value="<?php echo $row['act_city']; ?>">
                            </div>
</div>                            

                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="location">Area</label>
                                    <input type="text" class="form-control" id="act_area" name="act_area" value="<?php echo $row['act_area']; ?>">


                                </div>

                                <div class="form-group col-md-6">
                                    <label for="mobile">Address</label>
                                    <input type="text" class="form-control" id="act_address" name="act_address" value="<?php echo $row['act_address']; ?>">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">State</label>
                                    <input type="text" class="form-control" id="act_state" name="act_state" value="<?php echo $row['act_state']; ?>">


                                </div>

                                <div class="form-group col-md-3">
                                    <label for="team_head">Pincode</label>
                                    <input type="text" class="form-control" id="act_pincode" name="act_pincode" value="<?php echo $row['act_pincode']; ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="reports_to">Type of Call</label>
                                    <select class="form-control" name="act_call_type" id="act_call_type" value="<?php echo $row['act_call_type']; ?>">

										<option>Quote Submit</option>
										<option>Invoice Submission</option>
										<option>Payment Followup</option>
										<option>Others</option>
									</select>
                                    
                                    <script>


$("select option").each(function(){
  if ($(this).text() == "<?php echo $row['act_call_type']?>")
    $(this).attr("selected","selected");
});

$("#act_call_type").on("change", function(e){

  if ($("#act_call_type").val() == "Others")
 {


    $(".optional_input").addClass('show');
    $(".optional_input").removeClass('hide');


   }

else if($("#act_call_type").val() != "Others"){
    $(".optional_input").removeClass('show');
    $(".optional_input").addClass('hide');


}
});

  if ($("#act_call_type").val() == "Others")
 {


    $(".optional_input").addClass('show');
    $(".optional_input").removeClass('hide');


   }



                                    </script>




                                </div>
                            </div>
                            <div class="optional_input">
                                <div class="form-group">
                                    
                            <label for="reports_to">Specify</label>
                            <input class="form-control" name="act_other" id="act_other" value="<?php echo $row['act_other']; ?>" style=""/>
                                </div>
                            </div>





                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="location">Contact Person Name</label>
                                    <input type="text" class="form-control" id="act_con_person" name="act_con_person" value="<?php echo $row['act_con_person']; ?>">


                                </div>

                                <div class="form-group col-md-6">
                                    <label for="mobile">Mobile Number</label>
                                    <input type="number" class="form-control" id="act_mobile" name="act_mobile" value="<?php echo $row['act_mobile']; ?>"> 
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="location">Remarks</label>
                                    <input type="text" class="form-control" id="act_remarks" name="act_remarks" value="<?php echo $row['act_remarks']; ?>">


                                </div>

                                <div class="form-group col-md-6">
                                    <label for="mobile">Status</label>
                                    <select class="form-control" name="act_status" id="act_status" value="<?php echo $row['act_status']; ?>">

										<option>Pending</option>
										<option>complete</option>
									</select>
                                    <script>
$("select option").each(function(){
  if ($(this).text() == "<?php echo $row['act_status']?>")
    $(this).attr("selected","selected");
});
</script>


                                </div>
                            </div>



                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        </form>


  <?php }  $result -> free_result();
} ?>


                    </div>


</div>


</main>


<script>

$(window).ready(function() {

    $('form').submit(function() {
        if ($('#act_call_type').val() == '' || $('#act_status').val() == '' || $('#act_name').val() == '') {
            
            if ($('#act_status').val() == '') {
                $('#act_status').addClass('is-invalid');
            }

             if ($('#act_call_type').val() == '') {
                $('#act_call_type').addClass('is-invalid');
            }

                        if ($('#act_name').val() == '') {
                $('#act_name').addClass('is-invalid');
            }

            swal('please enter valid Input');

            return false;
        }

        return true;
    });
});


</script>
</body>
</html>
